:orphan:

Premiers pas en codage
====================================


.. admonition:: Objectifs
   :class: note

   * Savoir lire/écrire un diagramme de classe simple
   * Savoir écrire/compiler le code JAVA d'une classe simple
   * Savoir écrire/compiler/exécuter le code JAVA d'un exécutable

   
.. toctree::
   :maxdepth: 2
   :glob:

   exercices/*
