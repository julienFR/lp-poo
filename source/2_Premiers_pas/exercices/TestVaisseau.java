import junit.framework.*;

public class TestVaisseau extends TestCase{

    private String [] noms = {"a", "", "toto", "gimli", "legolas"};
    private int [] passagers = {65, 0, 23, 14, 54};
    private int [] puissance = {15, 10, 14, 7, 2};
        

    public void testTout() throws Exception { 
        for(int i = 0; i<5; i++)
        {
            Vaisseau v  = new Vaisseau(noms[i], passagers[i], puissance[i]);
            Vaisseau w = new Vaisseau(noms[i], puissance[i]);
            
            assertEquals( "La méthode getNom ne semble pas correcte",noms[i],v.getNom());
            assertEquals( "La méthode getNom ne semble pas correcte",noms[i],w.getNom());
            
            assertEquals( "La méthode nbPassagers ne semble pas correcte",passagers[i],v.nbPassagers());
            assertEquals( "La méthode nbPassagers ne semble pas correcte",0,w.nbPassagers());
            
            assertEquals( "La méthode getPuissance ne semble pas correcte",puissance[i],v.getPuissance());
            assertEquals( "La méthode getPuissance ne semble pas correcte",puissance[i],w.getPuissance());     

            assertEquals("La méthode transportDePassagers ne semble pas correcte",v.transportDePassagers(),passagers[i]>0);
            
            assertFalse( "La méthode transportDePassagers ne semble pas correcte",w.transportDePassagers());        
        }
    }
}

