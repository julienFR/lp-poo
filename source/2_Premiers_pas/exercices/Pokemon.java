public class Pokemon{
    private String nom;
    private int pv;
    private String type;
    
    public Pokemon(String nom, int pv){
        // this.nom = nom;
        // this.pv = pv;
        // this.type = "eau";
        this(nom, pv, "eau");
    }

    public Pokemon(String nom, int pv, String type){
        this.nom = nom;
        this.pv = pv;
        this.type = type;
    }

    public String getNom(){
        return this.nom;
    }
    
    public int getPV(){
        return this.pv;
    }
    
    public String getType(){
        return this.type;
    }
}
