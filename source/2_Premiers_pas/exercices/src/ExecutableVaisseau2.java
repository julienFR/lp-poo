class ExecutableVaisseau2{
  public static void main(String [] args){
    // Faucon Millenium est un Cargo qui peut transporter 6 passagers et dont la puissance de feu est de 4
    Vaisseau faucon = new Vaisseau("Faucon Millenium",6,4);
    System.out.println(faucon.getNom()); // Faucon Millenium
    System.out.println(faucon.nbPassagers()); // 6
    System.out.println(faucon.puissance()); // 4
    
    Vaisseau tie = new Vaisseau("Chasseur Tie",0,15);
    Vaisseau executor = new Vaisseau("Super Star Destroyer",38 000,250);
    Vaisseau corvette = new Vaisseau("Corvette",80,2);
    
    Flotte empire = new Flotte();
    empire.ajoute(tie);
    empire.ajoute(executor);
    System.out.println(empire.getNom()); // Nouvel flotte
    System.out.println(empire.nombreVaisseaux()); // 2
    System.out.println(empire.totalPuissance()); // 265 (250 + 15)
       
    Flotte alliance = new Flotte("Alliance rebelle");
    alliance.ajoute(faucon);
    alliance.ajoute(tie);
    alliance.ajoute(executor);
    alliance.ajoute(corvette);
    System.out.println(alliance.getNom()); // Alliance rebelle
    System.out.println(alliance.nombreVaisseaux()); // 4
    System.out.println(alliance.totalPuissance()); // 271 (4+15+250+2)
  }
}
