
public class Personnage{

    private String nom;
    private int tailleBarbe;
    private int tailleOreilles;
    
    public Personnage(String nom, int barbe, int oreilles){
        this.nom = nom;
        this.tailleBarbe = barbe;
        this.tailleOreilles = oreilles;
    }  
    
    public String getNom(){return this.nom;}
    public int getBarbe(){return this.tailleBarbe;}
    public int tailleOreilles(){return this.tailleOreilles;}

}
