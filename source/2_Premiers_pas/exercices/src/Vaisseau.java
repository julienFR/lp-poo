
public class Vaisseau{

    private String nom;
    private int nombreDePassagers;
    private int puissanceDeFeu;
    
    Vaisseau(String nom, int passagers, int puissance){
        this.nom=nom;
        this.nombreDePassagers=passagers;
        this.puissanceDeFeu=puissance;
    }

    Vaisseau(String nom, int puissance){
        // this.nom=nom;
        // this.nombreDePassagers=0;
        // this.puissanceDeFeu=puissance;
        this(nom, 0, puissance);
    }
    
    
    String getNom(){return nom;}
    int nbPassagers(){return nombreDePassagers;}
    int getPuissance(){return puissanceDeFeu;}
    boolean transportDePassagers(){
        // if (nombreDePassagers >0)
            // return true;
        // else
            // return false;
        return (nombreDePassagers >0);
    }

}
