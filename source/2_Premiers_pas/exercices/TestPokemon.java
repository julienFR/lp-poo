import junit.framework.*;

public class TestPokemon extends TestCase{
    
    private String [] nom = {"a", "toto", "gimli", "legolas"};
    private int [] attaque = {65, 0, 14, 54};
    private int [] type = {"eau", "terre", "feu", "terre"};
        
    public void testsBasiques() throws Exception {
        for(int i = 0; i<5; i++){
            Pokemon p1 = new Pokemon(nom[i], attaque[i]);
            Pokemon p2 = new Pokemon(nom[i], attaque[i], type[i]);
            
            assertEquals(nom[i],p1.getNom());
            assertEquals(nom[i],p2.getNom());
            
            assertEquals(50,p1.getPV());
            assertEquals(50,p2.getPV());

            assertEquals(attaque[i],p1.getAttaque());
            assertEquals(attaque[i],p2.getAttaque());

            assertEquals("eau",p1.getType());
            assertEquals(type[i],p2.getType());
        }
    }
    
