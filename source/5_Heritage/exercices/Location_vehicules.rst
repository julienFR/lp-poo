.. raw:: html

   <span class="exo"></span>


Agence de location de véhicules
+++++++++++++++++++++++++++++++++

**Objectif** : *Factorisation en utilisant l'héritage*



.. figure:: ../../images/jetSki.png
   :height: 4em
   :align: right

.. figure:: ../../images/bateau.png
   :height: 4em
   :align: right
   
.. figure:: ../../images/camion.jpeg
   :height: 4em
   :align: right
   
.. figure:: ../../images/voiture.png
   :height: 4em
   :align: right
   

Une première analyse a conduit à établir les classes ci-dessous pour la modélisation d'une agence de location de véhicules à moteurs en tous genres.



.. uml::

   @startuml
      
   skinparam classAttributeIconSize 0
   class AgenceDeLocation {
   - nom : String
   - adresse : String
   }

   class Voiture {
   - numeroImmatriculation : String
   - marque : String
   - cinqPortes : boolean
   - nombrePlaces : int
   + faireLePlein()
   + rouler()
   }
   
   class Camion {
   - numeroImmatriculation : String
   - marque : String
   - nombrePlaces : int
   - capaciteChargement : int
   + faireLePlein()
   + rouler()
   }

   class JetSki {
   - numeroImmatriculation : String
   - marque : String
   - puissance : int
   + faireLePlein()
   + naviguer()
   }
   
   class Bateau {
   - numeroImmatriculation : String
   - marque : String
   - nombrePlaces : int
   + faireLePlein()
   + naviguer()
   }
   
   AgenceDeLocation "1" --> "*" Voiture
   AgenceDeLocation "1" --> "*" Camion
   AgenceDeLocation "1" --> "*" JetSki
   AgenceDeLocation "1" --> "*" Bateau
   @enduml



.. figure:: ../../images/essence.png
   :height: 5em
   :align: right
   
   
#. Utilisez l’héritage pour optimiser le modèle. Vous pouvez introduire de nouvelles classes si nécessaire.

   On supposera que les associations introduites donnent lieu à des accesseurs (getters, setters) sans qu’il soit nécessaire de les préciser tous sur le diagramme.

#. On souhaite modifier le modèle pour pouvoir y ajouter des véhicules électriques, avec par exemple les classes `VeloElectrique`, `VoitureElectrique`, `JetSkiElectrique` ou `BateauElectrique` ci-dessous. Que proposez vous ?

   .. figure:: ../../images/batterie.jpeg
      :height: 5em
      :align: right


   .. uml::

      @startuml
         
      skinparam classAttributeIconSize 0
      
      class VeloElectrique {
      - numeroImmatriculation : String
      - marque : String
      - nombrePlaces : int
      - portebagage : boolean
      + recharger()
      + rouler()
      }

      class VoitureElectrique {
      - numeroImmatriculation : String
      - marque : String
      - cinqPortes : boolean
      - nombrePlaces : int
      + recharger()
      + rouler()
      }
      
      class JetSkiElectrique {
      - numeroImmatriculation : String
      - marque : String
      - puissance : int
      + recharger()
      + naviguer()
      }

      class BateauElectrique {
      - numeroImmatriculation : String
      - marque : String
      - nombrePlaces : int
      + recharger()
      + naviguer()
      }
      
      @enduml


   .. figure:: ../../images/avion.jpeg
      :height: 5em
      :align: left
      

   .. figure:: ../../images/hydravion.png
      :height: 5em
      :align: right

#. Et maintenant si on veut ajouter des véhicules volants ?

#. Comment faire pour pouvoir gérer des hydravions ? 


   

