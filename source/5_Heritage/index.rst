:orphan:

Héritage
---------------------

.. admonition:: Objectifs
   :class: note

   * Lire et concevoir un diagramme de classes (suite)
   * Aborder la notion d'héritage


.. toctree::
   :maxdepth: 2
   :glob:

   exercices/*


