.. raw:: html

   <span class="exo"></span>


Weekend entre amis
---------------------

.. class:: Consolider les notons abordées précédemment en résolvat un petit problème


.. figure:: ../../images/pingouincalcul.png
   :height: 5em
   :align: right




Le problème
+++++++++++

On est à la fin d’un week-end entre amis, et on cherche à faire les comptes suite aux dépenses de chacun.

.. figure:: ../../images/pingouinPainVin.jpg
   :height: 5em
   :align: left
   

Par exemple, Pierre a acheté du pain pour 12 euros, Paul a dépensé 100 euros pour les pizzas, Pierre a payé l’essence et en a eu pour 70 euros, Marie a acheté du vin pour 15 euros, Paul a aussi acheté du vin et en a eu pour 10 euros, Anna n’a quant à elle rien acheté.


#. Quelle somme totale a été dépensée pendant ce week end ?
#. Calculer la somme dépensée par chacun des participants
#. Calculer la somme que chaque personne doit verser/recevoir pour équilibrer les dépenses.


Analyse du problème : élaboration d'un diagramme de classes
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

On vous propose de créer les classes : ``Personne``, ``Depense`` et ``WeekEnd``.

#. Quels attributs doivent avoir les différentes classes pour pouvoir représenter les données ?

#.  Quelles méthodes et avec quels profils devez vous ajouter pour pouvoir :

   a) créer une personne, créer une dépense, créer un week end ;
   #) ajouter une personne ou une dépense à un week end ;
   #)  savoir combien a dépensé une personne ;
   #)  savoir combien d’argent a été dépensé pour un produit donné.
   #)  déterminer la somme que chaque personne doit verser/recevoir pour équilibrer les dépenses ?


Elaborez un diagramme de classe pour cette application


Codage d'une modélisation
++++++++++++++++++++++++++++++

On propose la modélisation suivante :

.. uml::

   @startuml
      
   skinparam classAttributeIconSize 0
   class Personne {
   - nom: String
   + Personne(nom:String)
   }

   class Depense {
   - montant: double
   - produit : String
   + Depense(payeur:Personne, montant:double, produit:String)
   }
   
   class WeekEnd {
   + WeekEnd()
   + ajoutePersonne(personne:Personne):void
   + ajouteDepense(payeur:Personne, montant:double, produit:String):void
   + totalDepensesPersonne(personne:Personne):double
   + totalDepenses():double
   + depenseMoyenne():double
   + equilibrer(personne:Personne):double
   }

   Depense "-payeur" --> "1" Personne
   WeekEnd "-listeDesDepenses" --> "*" Depense
   WeekEnd "-listeDesPersonnes" --> "*" Personne
   
   @enduml


