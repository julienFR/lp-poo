.. raw:: html

   <span class="exo"></span>


On branche avec la vue
++++++++++++++++++++++++


On vous donne du code qui gère l'affichage. Récupérez le fichier zip suivant : :download:`fichiers_pour_etu.tar.gz <fichiers_pour_etu/fichiers_pour_etu.tar.gz>`

Il contient :

* un dossier ``images`` contenant les images utiles à l'affichage,

* un dossier ``doc`` contenant des informations sur les classes que vous devez implémenter. Vous ouvrirez le fichier ``index.html`` pour lire cette documentation.


* un dossier ``src`` contenant les fichiers ``Executable.java`` (l'exécutable), ``Controleur.java`` et ``Vue_JeuDeCartes.java``.


#. Vérifier que l'ensemble compile et éxecuter le programme.

   Vous devez avoir un affichage ressemblant à la capture suivante. Les boutons ne fonctionnent alors pas.

.. image:: capture0.png
  :width: 80%




