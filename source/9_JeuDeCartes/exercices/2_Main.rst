.. raw:: html

   <span class="exo"></span>



Une main de cartes 
+++++++++++++++++++

Récupérez le fichier  :download:`Main.java <fichiers_pour_etu/Main.java>`.

La classe ``Main`` permet de modéliser une main, 
c'est à dire une liste de cartes et des méthodes associées. 

#. Compléter le constructeur

#. Compéter la méthode ``getMesCartes()``.






