import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Random;
import java.util.function.Function;
import java.util.function.ToDoubleFunction;
import java.util.function.ToIntFunction;
import java.util.function.ToLongFunction;


public class Main {
        
    private ArrayList<Jouable> main;
    private Jouable carteSelectionnee;

    public Main(){
        this.main = new ArrayList<>();

    }
        
    public ArrayList<Jouable> getMesCartes(){
        return this.main;
    }

    public Jouable getCarteSelectionnee(){
        return this.carteSelectionnee;
    }
    
    public void add(Jouable c){
        this.main.add(c);
    }
    
    public int nombreDeCartes(){
        return this.main.size();
    }
//      
//     public boolean contient(String couleur){
//       boolean trouve = false;
//              for (Carte c : main){
//                      if (c.getCouleur().equals(couleur))
//                              trouve = true;
//                      // else rien
//              }
//              return trouve;
//      }

    public boolean contient(String couleur){
        for (Jouable c : this.main){
            if (c.getCouleur().equals(couleur))
                return true;
            // else rien
        }
        return false; // J'arrive à cette ligne seulement j'ai parcourru
        // toute la liste sans trouver la couleur
    }

    public int sommeValeurs(){
        int total=0;
        for (Jouable c : this.main)
            total+=c.getValeurInt();
        return total;
    }
        
    @Override
    public String toString(){
        return this.main.toString();
    }
    
    public void jouer(){
        if (this.carteSelectionnee!=null)
        {
            this.main.remove(carteSelectionnee);
            this.carteSelectionnee=null;
        }
        else
            System.out.println("aucune carte selectionnée");
        }

        // public void jouerToutesCouleurs(){
        // String couleur=carteSelectionnee.getCouleur();
                // ArrayList<Carte> aJeter = new ArrayList<>();
                // for (Carte c:main)
                        // if (c.getCouleur().equals(couleur))
                                // aJeter.add(c);
                
        // for (Carte c:aJeter)
                        // main.remove(c);      
        
        // this.carteSelectionnee=null;
        // }


    public void jouerToutesCouleurs(){
        String couleur=this.carteSelectionnee.getCouleur();
        ArrayList<Jouable> aGarder = new ArrayList<>();
        for (Jouable c: this.main)
            if (! c.getCouleur().equals(couleur))
                aGarder.add(c);
        this.main=aGarder;
        this.carteSelectionnee=null;
    }
        // public void jouerToutesValeurs(){
        // String valeur=carteSelectionnee.getValeur();    
                // ArrayList<Carte> aJeter = new ArrayList<>();
                // for (Carte c:main)
                        // if (c.getValeur().equals(valeur))
                                // aJeter.add(c);
                // for (Carte c:aJeter)
                        // main.remove(c);      
        
        // this.carteSelectionnee=null;
        // }

    public void jouerToutesValeurs(){
        String valeur=carteSelectionnee.getValeur();    
        ArrayList<Jouable> aGarder = new ArrayList<>();
        for (Jouable c : this.main)
            if (! c.getValeur().equals(valeur))
                aGarder.add(c);
        this.main=aGarder;
        this.carteSelectionnee=null;
    }    
        
    public void triParValeurs(){
        Collections.sort(main);
    }
        /**
         * Trie dans l'ordre "trefle" < "carreau"< "coeur" < "pique"
         */
    public void triParCouleurs(){
        class CompareCouleurs implements Comparator<Jouable>{
            private int ordre(Jouable c){
                int ordre;
                if (c.getCouleur().equals("trefle"))
                    ordre=1;
                else if (c.getCouleur().equals("carreau"))
                    ordre=2;
                else if (c.getCouleur().equals("coeur"))
                    ordre=3;
                else
                    ordre=4;
                return ordre;
            }
                        
            @Override
            public int compare(Jouable c1, Jouable c2) {
                return ordre(c1)-ordre(c2);
            }
        }
        Collections.sort(main, new CompareCouleurs());
    }

    public void triParCouleursEtValeurs(){
        this.triParValeurs();
        this.triParCouleurs();
    }

    public void piocher(){
        Random generateurDeNombresAleatoires = new Random();
        String [] couleurs={"trefle", "carreau", "coeur", "pique"};
        int valeur = generateurDeNombresAleatoires.nextInt(13)+1;
        String couleur = couleurs[generateurDeNombresAleatoires.nextInt(4)];

        this.add(new Carte(valeur, couleur));
        System.out.println(this.main);
        }
    
    public void setCarteSelectionee(Jouable c)
    {
        this.carteSelectionnee=c;
    }

    public boolean carteSelectionnee()
    {
        return !(this.carteSelectionnee==null);
    }

}
