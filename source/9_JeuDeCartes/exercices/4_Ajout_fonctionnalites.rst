.. raw:: html

   <span class="exo"></span>


Ajout de fonctionnalités
++++++++++++++++++++++++++


#.  Ajoutez le code pour que la méthode ``add`` de Main soient fonctionnelle. 

   Vous devez alors avoir quelque chose comme :


.. image:: capture.png
  :width: 80%


#. Dans l'ordre, écrivez les méthodes :

   * ``setCarteSelectionee``,

   * ``getCarteSelectionnee``,

   * ``jouerToutesValeurs`` et ``jouerToutesCouleurs``. 
     Attention, ne supprimez pas une valeur d'une liste que vous êtes en train de parcourir. Vous pouvez par exemple commencer par créer la liste des cartes à supprimer, puis supprimez ces cartes. Ou bien vous pouvez commencez par créer la liste des cartes à garder, puis assignez cette valeur à la liste des cartes de votre main.


#. Écrivez la méthode ``triParValeurs``.

   Pour cette méthode, on utiliser l'*ordre naturel* ce qui signifie qu'il faut implémenter l'interface *Comparable*. 
   Mais dans quelle classe ?

#. Écrivez la méthode ``triParCouleur``. 
   Pour cette méthode, on utilisera un comparateur, ce qui signifie qu'il faut implémenter l'interface *Comparator*.






