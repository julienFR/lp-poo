:orphan:


Jouer aux cartes
--------------------------

S'il vous est déjà arrivé de jouer aux cartes, vous avez probablement :

* ajouté des cartes dans votre main,

* trié ces cartes selon un critère bien à vous,

* joué les cartes, soit une par une comme à la *belote*, soit par plusieurs d'un coup comme dans le jeu du *président* par exemple.



Dans cette séquence vous allez créer un simulateur de *Main*, c'est à dire un outil qui permette de créer une main (piocher des cartes), jouer une ou plusieurs cartes, trier les cartes.. 

   
.. admonition:: Objectifs
   class:notes
   
   * Consolider les notions abordées précédemment
   * Aborder la notion de tri (trier une collection)
   
   *Mots clés* : interface `Comparable`, interface `Comparator`, les deux méthodes ``sort`` de la bibliothèque `Collections`




.. figure:: ../images/card-deck-161536_960_720.png
   :width: 100%
   :align: center


.. toctree::
   :maxdepth: 2
   :glob:

   exercices/*


