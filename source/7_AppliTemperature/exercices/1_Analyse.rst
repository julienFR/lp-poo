.. raw:: html

   <span class="exo"></span>


Diagramme de classes
++++++++++++++++++++++++++++

**Exercice Version apprenti jedi**


On vous propose une modélisation dans laquelle la vue est composée des classes **Application**, **AppliTemperature**,  **BoiteTempérature**,  **Slider**,  **TextField** et **TitledPane**

#. Parmi ces classes, quelles sont celles qui viennent de l'API java ?

   .. admonition:: Notes
      class: notes
      
       * La documentation de l'API Java SE se trouve là : 
         https://docs.oracle.com/javase/8/docs/api/index.html
       * La documentation de l'API JavaFX se trouve là : 
         https://docs.oracle.com/javase/8/javafx/api/toc.htm


   
   
#. Donnez la liste des autres classes qui vous sembleraient pertinentes pour coder cette application. Organisez-les en trois catégories : celles qui appartiennent au **Modèle**, celles qui  appartiennent à la  **Vue** et celles qui appartiennent au **Contrôleur**.

#. Organisez-les dans un diagramme de classes avec les associations qui vous paraissent pertinentes.


#. Anticipez les attributs et méthodes qui seront nécessaires.


**Exercice Version jeune padawan**
   
Le code (incomplet) de cette application se trouve dans le répertoire ``/pub/LP/JavaPOO/AppliTemperature/``

Récupérer le.


1. Parmi les classes proposées, identifier celles qui appartiennent au **Modèle**, celles qui appartiennent à la **Vue** et celles qui appartiennent au **Contrôleur**.

#. Esquissez un diagramme de classes avec les méthodes qui vous paraissent pertinentes.


