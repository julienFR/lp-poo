
 
public class GestionFarenheit extends GestionTemperatures{

    public GestionFarenheit(CalculTemperature temperature, AppliTemperatures appli ){
        super(temperature,appli);
    }
    
    @Override
    void modifieTemperature() throws NumberFormatException, TemperatureNonValide
    {
        this.temperature.setFarenheit(this.appli.getFarenheit());
    }
    
}
 
