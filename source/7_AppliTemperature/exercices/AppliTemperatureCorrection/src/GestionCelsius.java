
public class GestionCelsius extends GestionTemperatures{
    
    public GestionCelsius(CalculTemperature temperature, AppliTemperatures appli ){
        super(temperature,appli);
    }
    @Override
    void modifieTemperature() throws NumberFormatException, TemperatureNonValide
    {
        this.temperature.setCelsius(this.appli.getCelsius());

    }
}
