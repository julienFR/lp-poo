import javafx.application.Application;
import javafx.stage.Stage;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.control.Slider;
import javafx.geometry.Orientation;

public class AppliTemperatures extends Application {


    private Slider slider;
    private TextField temp_celsius_affichee;
    private TextField temp_farenheit_affichee;

    // Exercice 3
    // AJOUTEZ ICI LES ATTRIBUTS QUI VOUS SEMBLENT NECESSAIRES
    private CalculTemperature temperature;


    /** Met à jour l'affichage des températures */
    public void majAffichage(){
        // Exercice 3
        // COMPLETER LE CODE DE CETTE METHODE
        this.temp_celsius_affichee.setText(this.temperature.getCelsius()+"");
        this.temp_farenheit_affichee.setText(this.temperature.getFarenheit()+"");
        this.slider.setValue(this.temperature.getCelsius());
    }

    /** récupère la température en Celsius qui se trouve dans le TextField
     * @exception NumberFormatException si l'information entrée ne correcpond pas à un nombre
     */
    public double getCelsius() throws NumberFormatException
    {
        return Double.parseDouble(temp_celsius_affichee.getText());
    }

    /** récupère la température en Farenheit qui se trouve dans le TextField
     * @exception NumberFormatException si l'information entrée ne correcpond pas à un nombre
     */
    public double getFarenheit() throws NumberFormatException
    {
        return Double.parseDouble(temp_farenheit_affichee.getText());
    }

    /**
     * Récupère la température en Celsius donnée par le Slider
     */
    public double getValueSlider(){
        // Exercice 2 - Question 2.5
        // COMPLETER LE CODE DE CETTE METHODE
        return this.slider.getValue();
    }

    /** renvoie la scene */
    private Scene laScene(){
        HBox cont = new HBox(10);
        cont.setAlignment(Pos.CENTER);
        VBox vbox = new VBox(10);
        vbox.setAlignment(Pos.CENTER);
        cont.getChildren().add(vbox);
        vbox.getChildren().add(new BoiteTemperature("Température en Celsius", this.temp_celsius_affichee));
        vbox.getChildren().add(new BoiteTemperature("Température en Farenheit", this.temp_farenheit_affichee));
        cont.getChildren().add(new BoiteTemperature("Température en Celsius", this.slider));
        return new Scene(cont,500,300);
    }

    /**
     * Met en forme la Scene principale
     * @param stage la fenêtre principale
     */
    @Override
    public void start(Stage stage) {
        stage.setTitle("Contrôle de la température");
        stage.setScene(this.laScene());
        stage.show();
        this.majAffichage();
    }

    /**
     * Instancie tous les attributs 
     * et connecte les widgets aux contrôleurs
     */
    @Override
    public void init(){
        // Exercice 2 - Question 2.3
        // INSTANCIER ICI LES ATTRIBUTS
        this.temp_celsius_affichee = new TextField();
        this.temp_farenheit_affichee =  new TextField();
        this.slider = new Slider(-10, 80, 10);
        //this.slider = new Slider(this.temperature.getCelsiusMin(), this.temperature.getCelsiusMax(), 10);

        // Exercice 2 - Question 2.4
        // REGLAGES DU SLIDER
        this.slider.setShowTickMarks(true);
        this.slider.setShowTickLabels(true);
        this.slider.setMajorTickUnit(10);
        this.slider.setBlockIncrement(5);
        this.slider.setOrientation(Orientation.VERTICAL);


        //Exercice 3
        // INSTANCIER ICI LES ATTRIBUTS RESTANTS
        this.temperature = new CalculTemperature();


        // Exercice 4
        // ON "CONNECTE" les widgets avec leur controleurs rspactifs

        this.temp_celsius_affichee.setOnAction(new GestionCelsius(this.temperature, this));

        this.temp_farenheit_affichee.setOnAction(new GestionFarenheit(this.temperature, this));


        this.slider.valueProperty().addListener(new GestionSlider(this.temperature, this));


    }

    /**
     * Programme principal
     * @param args inutilisé
     */
    public static void main(String[] args) {
        launch(args);
    }

}
