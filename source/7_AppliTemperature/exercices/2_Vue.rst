.. raw:: html

   <span class="exo"></span>


La vue
++++++++++++++++++++++++++++


Le code (incomplet) de cette application se trouve dans le répertoire ``/pub/LP/JavaPOO/AppliTemperature/``.

Récupérer le.


.. admonition:: Remarque
   :class: note
   
   javafx n'est plus intégré nativement dans java mais il est fourni sous la forme de modules.
   Ainsi,
   
   * pour compiler, il faut ajouter les options suivantes :
     ``javac --module-path /usr/share/openjfx/lib/ --add-modules javafx.controls *.java``
     
   * pour exécuter, il faut ajouter les options suivantes :
     ``java --module-path /usr/share/openjfx/lib/ --add-modules javafx.controls MonApplication``
   
   Paramétrer IntelliJ,

   * Vérifier les SDK et JRE utilisés (version 11)
   
   * pour compiler  **File > Project Structure > Libraries** puis ajouter deux librairies (avec le petit +)
     ``/usr/share/openjfx/lib/`` et  ``/usr/share/openjfx/lib/javafx.controles.jar``

   * pour exécuter, **Run > Edit Configuration > Application** et ajouter dans le textfield 
   
     **VM options :** ``--module-path /usr/share/openjfx/lib/ --add-modules javafx.controls``



**La classe AppliTemperature**

La classe **AppliTemperature** est incomplète. Les questions suivantes devraient vous permettre de la compléter.

#. Dans la méthode ``init()``, instanciez les attributs.

   Une fois que vous l'avez fait, décommentez les trois lignes de la méthode ``laScene()`` puis compilez et exécutez à nouveau le code.


**Réglages du slider**

On voudrait que le slider soit gradué de -40 jusqu'à 100 en allant de 10 en 10.
   
#. Quel constructeur utiliser ?

#. Ajoutez les lignes de code suivantes. A quoi servent-elles ?
   
   .. code:: java
      
      this.slider.setShowTickMarks(true);
      this.slider.setShowTickLabels(true);
      this.slider.setMajorTickUnit(10);
      this.slider.setBlockIncrement(5);

#. Ajouter l'instruction qui permet de mettre le slider à la verticale.
   

#. Complétez le code de la méthode ``getValueSlider()``

#. Dans la méthode ``getCelcius()`` , identifiez toutes les méthodes invoquées en précisant à chaque fois :

   * la classe à laquelle elle appartient ;
   * ce que fait cette méthode.


