:orphan:

.. raw:: latex
   
   \renewcommand{\thesection}{Exercice \arabic{section}.}
   \renewcommand{\thesubsection}{\alph{subsection} :}
   \renewcommand\chaptername{Evaluation}
   

LP - Programmation Orientée Objet
==================================

.. toctree::
   :maxdepth: 2
   :numbered: 2
   :glob:
   
   Evaluations*/__index



