import java.lang.String;

// Pour générer la javadoc, avec les accents ok, sans la précision des packages
// javadoc -charset utf8 -noqualifier all -d doc Timer.java
public class Timer {
    /**
     * @author prof IUT
     */
    private double periode;
    private boolean actif;

    /** Créer un Timer inactif avec une période de 10 secondes */
    public Timer() {
        this(10);
    }
    /** 
     * Créer un Timer inactif 
     * @param periode est la période du Timer créé, exprimée en secondes
     */
    public Timer(double periode) {
        this.periode = periode;
        this.actif=false;
    }
    /**
    * @return la période du Timer, exprimée en secondes
    */    
    public double getPeriode() {
        return this.periode;
    }
    /**
    * @param nouvellePeriode la nouvelle période du Timer doit être
    * exprimée en secondes
    */  
    public void setPeriode(double nouvellePeriode) {
        this.periode=nouvellePeriode;        
    } 
    public void activer() {
        this.actif=true;
    }
    public void desactiver() {
        this.actif=false;
    }
    public void utiliser() {
        if (actif)
            try{ Thread.sleep((long) periode);}
            catch(Exception e){}
    }
    // public String toString() {
        // return "Timer [periode=" + periode + ", actif=" + actif + "]";
    // }
}
