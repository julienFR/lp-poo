/** Cette classe modélise un point sur un quadrillage **/
public class PointDuPlan {
    
    private int abscisse;
    private int ordonnee;
    
    /** Construit un point à l'origine du repère **/
    public PointDuPlan() {
        this.abscisse = 0 ;
        this.ordonnee = 0 ;
    }
    public PointDuPlan(int x, int y) {
        this.abscisse = x ;
        this.ordonnee = y ;
    }
    public int getOrdonnee() {
        return this.ordonnee ;
    }
    public int getAbscisse() {
        return this.abscisse ;
    }
    public boolean egaleA(PointDuPlan autrePoint) {
        return (this.abscisse == autrePoint.abscisse 
                && this.ordonnee ==  autrePoint.ordonnee) ;
    }
    public void translate(int dx, int dy) {
        this.abscisse = this.abscisse + dx ;
        this.ordonnee = this.ordonnee + dy ;
    }
    /** renvoie la distance entre deux points **/
    public double distance(PointDuPlan autrePoint) {
        return Math.sqrt((this.abscisse-autrePoint.abscisse)*(this.abscisse-autrePoint.abscisse)+(this.ordonnee-autrePoint.ordonnee)*(this.ordonnee-autrePoint.ordonnee)) ;
    }
    public String toString() {
        return "("+this.getAbscisse()+","+this.getOrdonnee() + ")";
    }
    public void affiche() {
        System.out.println(this.toString())  ;
    }
}
