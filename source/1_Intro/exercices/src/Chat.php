<?php
/** Classe Chat en PHP */

class Chat{

  /** Nom du chat */
  private $nom;

  /** Constructeur de chat */  
  public function __construct($nom){
    $this->nom = (string)$nom;     // cast vers string
  }
  
  // Accesseurs ===================

  public function getNom(){
    return $this->nom;
  }

  // Mutateurs ===================
  
  public function setNom($nouveauNom){
    $this->nom = (string)$nouveauNom;
  }

  // Autres méthodes ===================

  /**
   * Cette méthode fait miauler le chat en affichant un message
   * sur la sortie standard (dans le terminal par défaut)
   */
  public function miaule(){
    print "Miaou !!!!\n";
  }

  /**
   * Cette méthode détermine si le chat est endormi
 
   * @param heure un double compris entre 0 et 24 qui 
   * précise l'heure de la journée
   * @return true si le chat dort (presque tout le temps) et false sinon
   * (c'est à dire entre 3 et 4 heures du matin)
   */
  public function estEndormi(double $heure): boolean{
    return ($heure<=3 || $heure>4);
  }
}
