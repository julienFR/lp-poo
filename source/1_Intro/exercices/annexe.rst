.. raw:: latex

   \newpage
   

Annexe
++++++++++++++++++++++++++++++

Le diagramme de classe est indépendant du langage objet utilisé pour le code.

Certains logiciels IDE permettent de générer automatiquement le code à partir d'un diagramme de classes.

Par exemple, voici un diagramme de classe ainsi que son implémentation dans différents langages orientés objets :


.. uml::

   @startuml
      
   skinparam classAttributeIconSize 0
   class Chat {
   
   - nom: String
   + Chat(nom: String)
   + setNom(nouveauNom: String): void
   + miaule(): void
   + estEndormi(heure: double): boolean
   }
      
   @enduml
   

.. literalinclude:: ./src/Chat.java
   :language: java
   :caption: La classe `Chat` en JAVA


.. literalinclude:: ./src/Chat.py
   :language: python
   :caption: La classe `Chat` en PYTHON

   
.. literalinclude:: ./src/Chat.php
   :language: PHP
   :caption: La classe `Chat` en PHP

