.. raw:: html

   <span class="exo"></span>

La classe Point du Plan
+++++++++++++++++++++++

**Objectif** : *Construire un diagramme de classe simple à partir d'un code JAVA*


Voici le code JAVA d'une classe `PointDuPlan` :


.. literalinclude:: ./src/PointDuPlan.java
   :language: java


#. Combien y a-t-il de constructeurs ? 

#. Combien y a-t-il de méthodes ? 

#. Combien y a-t-il d'attributs ?

#. Donner une représentation UML de la classe `PointDuPlan`.

#. Donner un exemple d'instance de cette classe.

#. Expliquer ce que font les méthodes ``affiche()``, ``toString()`` et  ``translate()``. Donner au moins un exemple à chaque fois.



