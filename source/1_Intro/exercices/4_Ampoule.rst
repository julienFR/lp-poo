.. figure:: ../../images/ampoule.png
   :height: 3em
   :align: right   

.. raw:: html

   <span class="exo"></span>

La classe Ampoule
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


**Objectif** : *Construire un diagramme de classe simple à partir d'un code JAVA*


Voici le code JAVA d'une classe `Ampoule` :

.. literalinclude:: ./src/Ampoule.java
   :language: java


#. Donner une représentation UML de la classe `Ampoule`.

#. Donner deux exemples d'instance de cette classe (avec deux attributs `eteinte` différents). 

#. Expliquer ce que font les méthodes ``allumer()`` et ``toString()``. Donner au moins un exemple à chaque fois.
 
