.. raw:: html

   <span class="exo"></span>


Analyse du projet
++++++++++++++++++++++++++++


#. Donnez la liste des classes qui vous sembleraient pertinentes pour coder cette application. Organisez-les en trois catégories : celles qui appartiennent au **Modèle**, celles qui  appartiennent à la  **Vue** et celles qui appartiennent au **Contrôleur**.

#.  Organisez-les dans un diagramme de classes avec les associations qui vous paraissent pertinentes.


#.  Anticipez les attributs et méthodes qui seront nécessaires.

