.. raw:: html

   <span class="exo"></span>
   

La vue
++++++++++++++++++++++++++++

La vue est composée des classes `Application`, `AppliTemperature`,  `BoiteTempérature`,  `Slider`,  `TextField` et `TitledPane`

#. Parmi ces classes, quelles sont celles qui viennent de l'API java ?

.. admonition:: Indices
   :class: hint
   
   Liens à mettre en favori si ce n'est pas déjà fait :
   
      * La documentation de l'API Java SE se trouve là : `Doc Java <https://docs.oracle.com/javase/8/docs/api/index.html/>`_.

      *  La documentation de l'API JavaFX se trouve là : `Doc JavaFX <https://docs.oracle.com/javase/8/javafx/api/toc.htm/>`_.



#. Récupérez une partie des classes dans``/pub/LP/...``. Compilez et exécutez le code (versionnez).
   Esquissez un diagramme de classes avec les associations entre ces différentes classes.
   

La classe `AppliTemperature` est incomplète. Les questions suivantes devraient vous permettre de la compléter.

#. Dans la méthode ``init()``, instanciez les attributs.
   Une fois que vous l'avez fait, décommentez les trois lignes de la méthode ``laScene()`` puis compilez et exécutez à nouveau le code.

#. **Réglages du slider**
   On voudrait que le slider soit gradué de -40 jusqu'à 100 en allant de 10 en 10.

   a) Quel constructeur utiliser ?
   #) Ajoutez les lignes de code suivantes (au bon endroit). A quoi servent-elles ?
   
   .. code:: java
   
        this.slider.setShowTickMarks(true);
        this.slider.setShowTickLabels(true);
        this.slider.setMajorTickUnit(10);
        this.slider.setBlockIncrement(5);

   c) Ajouter une instruction qui permet de mettre le slider à la verticale.


#.  Complétez le code de la méthode ``getValueSlider()``

#. Dans la méthode ``getCelcius()`` , identifiez toutes les méthodes invoquées en précisant à chaque fois :

   * la classe à laquelle elle appartient ;
   * ce que fait cette méthode.

