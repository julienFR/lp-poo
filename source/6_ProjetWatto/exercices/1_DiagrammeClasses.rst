.. raw:: html

   <span class="exo"></span>


Diagramme de classes
++++++++++++++++++++++++++++


Dans un premier temps, on va mettre en place le modèle. C'est le coeur de l'application. Il regroupe tout ce qui sert à gérer les données et toute la partie algorithmique.

Voici des exemples de droïdes que Watto veut avoir dans son stock : 

.. figure:: ../../images/droid_dum.jpg
   :height: 5em
   :align: right
 
* Type : Droïde
* Nom : DUM
* Fonction : Droïde mécanicien
* Etat : bon état (50%) 


.. figure:: ../../images/droid_eg6.jpg
   :height: 5em
   :align: right
   

* Type : Droïd
* Nom : Gonk
* Fonction : Droïde énergatique
* Etat : presque neuf (80%)


Voici des exemples de vaisseaux que Watto veut avoir dans son stock : 

.. figure:: ../../images/ship_ixiyen.jpg
   :height: 5em
   :align: right
   

* Type : Vaisseau
* Nom : Vaisseau d'attaque rapide de classe Ixiyen
* Classe :Chasseur stellaire
* Etat : bon état (40%)

.. figure:: ../../images/ship_mante_d5.jpg
   :height: 5em
   :align: right
   

* Type : Vaisseau
* Nom : Vaisseau de Patrouille Mante D5
* Classe : Vaisseau de Combat léger
* Etat : presque neuf (60%)



Watto veut pouvoir **ajouter** et **vendre** des articles dans son stock. Comme il est très bon mécanicien, il veut également pouvoir améliorer leur état dans l'application s'il répare droïdes ou vaisseaux.

Proposer un diagramme de classe qui permettrait de modéliser la nouvelle boutique de Watoo.
(nom des classes, nom et type des attributs, profils des méthodes)

