.. raw:: html

   <span class="exo"></span>


Implémentation en Java
++++++++++++++++++++++++++++


Voici le diagramme de classes  auquel nous sommes arrivés. Comme il s'agit d'une première analyse, il est très certainement incomplet.


.. uml::

   @startuml
      
   skinparam classAttributeIconSize 0
   abstract class Article {
   # image : String
   + getImage() : String
   }

   interface Vendable {
   + getType(): String
   + getNom() : String
   + getEtat() : int
   + getInfos() : String
   + reparer() : void

   }
   
   class Stock {
   - propriétaire : String
   + Stock(proprietaire:String)
   + vendre(article:Vendable, acheteur:String) : void
   + ajouter(article:Vendable) : void
   + suivant() : void
   + precedent() : void
   }

   class Droide {
   - fonction : String
   + Droide(nom:String, etat:double, fonction:String, image:String)
   }
   
   class Vaisseau {
   - classe : String
   + Vaisseau(nom:String, etat:double, classe:String, image:String)
   }

   Article <|-- Droide
   Article <|-- Vaisseau
   Vendable <|.. Article
   
   Vendable "*" <-- "-leStock" Stock

   Vendable "1" <-- "-articleEnPresentation" Stock
   
   note left of Vendable
      l'état d'un article est un nombre entre 0 et 100
        0% = à mettre à la poubelle
        100% = état neuf
      La méthode reparer() augmente l'état de l'article de 10 points
      sans dépasser 100% 
   end note
   
   @enduml




#. Ecrivez le code **minimal** des classes et interfaces de ce diagramme. Pour le moment, les méthodes ne font rien ! 
   
   Puis vérifiez que votre projet compile.

#. Ecrivez un Exécutable qui vous premettra de tester une à une les fonctionnalités que vous coderez.
   
   Vérifiez que votre code s'exécute sans erreur (mais pour le moment, il ne fait rien)

#. Ajoutez **une à une** les fonctionnalités qui vous semblent necessaires. 

  Avant de coder la fonctionnalité suivante :
  
  * Ajouter du code dans l'exécutable qui vous permet de tester cette fonctionnalité (un ``assert`` ou un ``System.out.println( ...)`` à défaut)
  * Vérifier que votre code compile
  * Vérifier son exécution

