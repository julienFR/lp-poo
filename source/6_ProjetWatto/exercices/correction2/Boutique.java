import javafx.application.Application;
import javafx.stage.Stage;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.control.Slider;
import javafx.geometry.Orientation;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class Boutique extends Application {
    
    private Stock leStock;


    /** Met à jour l'affichage */
    public void majAffichage(){
        
    }


    /** renvoie la scene */
    private Scene laScene(){
            
        VBox vbox = new VBox(10);
        
        
        HBox hboxHaut = new HBox(10);
        HBox hboxMilieu = new HBox(10);
        HBox hboxBas = new HBox(10);
        vbox.getChildren().addAll(hboxHaut, hboxMilieu, hboxBas);

        // Boite du haut
        ComboBox<String> lesTypes = new ComboBox<>();
        lesTypes.getItems().addAll("Droide", "Vaisseau");
        
        HBox hboxSP = new HBox(10);
        hboxSP.getChildren().addAll(new Button("Precedent"), new Button("Suivant"));
        
        hboxHaut.getChildren().addAll(lesTypes, hboxSP, new Button("Ajouter un materiel"));

        // Boite du milieu
        VBox vboxInfos = new VBox(10);
        VBox vboxImage = new VBox(10);
        Image image =  new Image("test_plantuml.png", 100, 0, false, false);
        ImageView iv = new ImageView();
         iv.setImage(image);
        hboxMilieu.getChildren().addAll(iv, vboxInfos);
        
                
                
        return new Scene(vbox,500,300);
    }

    /**
     * Met en forme la Scene principale
     * @param stage la fenêtre principale
     */
    @Override
    public void start(Stage stage) {
        stage.setTitle("La boutique de Watto");
        stage.setScene(this.laScene());
        stage.show();
        this.majAffichage();
    }

    /**
     * Instancie tous les attributs 
     * et connecte les widgets aux contrôleurs
     */
    @Override
    public void init(){
        this.leStock = new Stock("La boutique de Watto");
    }

    /**
     * Programme principal
     * @param args inutilisé
     */
    public static void main(String[] args) {
        launch(args);
    }

}
