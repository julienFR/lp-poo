:orphan:

Projet Watoo
---------------------

**Présentation du projet**

.. figure:: ../images/perso_watto.jpg
   :height: 5em
   :align: right
   

Watto , célèbre et honnête marchand de Tatooine, a fait fortune avec son échoppe de pièces détachées.
Maintenant, il rêve d'ouvrir une nouvelle boutique dans laquelle il vendrait des droïdes et des vaisseaux .

Il vous charge de développer une application qui lui permettrait de gérer son stock.


   

**Maquette**



Watto fait appel aux meilleurs designers intergalactiques et voici leur proposition de maquette :

.. figure:: ../images/maquette.png
   :width: 100%
   :align: center
   


Cette maquette a été réalisée sur le site https://balsamiq.com/. Ce site est payant, mais Watto est très généreux et je peux créer un (ou plusieurs) projet(s) et vous inviter à y participer. Il me suffit de votre adresse mail.





.. toctree::
   :maxdepth: 2
   :glob:

   exercices/*


