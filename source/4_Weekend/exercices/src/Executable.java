

public class Executable {
    public static void main(String[] args) {
        Personne anna = new Personne("Anna");
        Personne barnabe = new Personne("Barnabé");
        Personne chris = new Personne("Chris");
        Personne pierre = new Personne("Pierre");
        Personne paul = new Personne("Paul");
        Personne marie = new Personne("Marie");

        WeekEnd aout = new WeekEnd("week end du 15 août");

        aout.ajouterParticipant(anna);
        aout.ajouterParticipant(barnabe);
        aout.ajouterParticipant(chris);

        aout.ajouterDepense(new Depense("cinéma", 20, anna));
        aout.ajouterDepense(new Depense("nourriture", 30, anna));
        aout.ajouterDepense(new Depense("boissons", 10, barnabe));
        aout.ajouterDepense(new Depense("essence", 15, chris));

        WeekEnd anniversairePierre = new WeekEnd("Anniversaire de Pierre");

        anniversairePierre.ajouterParticipant(pierre);
        anniversairePierre.ajouterParticipant(paul);
        anniversairePierre.ajouterParticipant(marie);
        anniversairePierre.ajouterParticipant(anna);

        anniversairePierre.ajouterDepense(new Depense("pain", 12, pierre));
        anniversairePierre.ajouterDepense(new Depense("pizzas", 100, paul));
        anniversairePierre.ajouterDepense(new Depense("essence", 70, pierre));
        anniversairePierre.ajouterDepense(new Depense("vin", 15, marie));
        anniversairePierre.ajouterDepense(new Depense("vin", 10, paul));

        System.out.println(aout.getLibelle()+" // Total du weekend : "+aout.totalWeekend());
        System.out.println("\nTotal par personne");
        for (Personne p : aout.getParticipants()) {
            System.out.println(p.getNom()+" : "+aout.totalParticipant(p));
        }

        System.out.println("\n"+aout.getLibelle()+" // Moyenne du weekend : "+aout.moyenne());
        System.out.println("\nEquilibrage");
        for (Personne p : aout.getParticipants()) {
            System.out.println(p.getNom()+" : "+aout.equilibre(p));
        }

        System.out.print("\n");

        System.out.println(anniversairePierre.getLibelle()+" // Total du weekend : "+anniversairePierre.totalWeekend());
        System.out.println("\nTotal par personne");
        for (Personne p : anniversairePierre.getParticipants()) {
            System.out.println(p.getNom()+" : "+anniversairePierre.totalParticipant(p));
        }

        System.out.println("\n"+anniversairePierre.getLibelle()+" // Moyenne du weekend : "+anniversairePierre.moyenne());
        System.out.println("\nEquilibrage");
        for (Personne p : anniversairePierre.getParticipants()) {
            System.out.println(p.getNom()+" : "+anniversairePierre.equilibre(p));
        }
    }
}

