import java.util.List; 
import java.util.ArrayList; 

public class WeekEnd
{
    /** Représente la liste des amis qui ont participé au WeeEnd */
    private List<Personne> listeAmis;

    /** Représente la liste des dépenses du WeeEnd */
    private List<Depense> listeDepenses;
    
    private String nomDuWe;
    
    public WeekEnd(String nomDuWe)
    {
	this.nomDuWe = nomDuWe;
	this.listeAmis = new ArrayList<Personne>();
	this.listeDepenses = new ArrayList<Depense>();
    }
    
    public void ajoutePersonne(Personne personne)
    {
        this.listeAmis.add(personne);
    }

   
    public void ajouteDepense(Personne payeur, double montant, String produit)
    {
        this.listeDepenses.add(new Depense(payeur, montant, produit));
    }
    
    /**
     * cette méthode prend en paramètre une personne et renvoie la somme des depenses de cette personne.
     */
    public double totalDepensesPersonne(Personne personne)
    {
	    double somme = 0.0;
	    for(Depense depense : this.listeDepenses)
        {
		// if (personne.estEgal(depense.getPersonne()))
	        if (personne.equals(depense.getPersonne()))
	            somme += depense.getMontant();
	    }
	    return somme;
    }
    
    /**
     * cette méthode renvoie la somme de toutes les dépenses.
     */
    public double totalDepenses()
    {
        double somme = 0.0; 
        for(Depense depense : this.listeDepenses) 
            somme += depense.getMontant();
        return somme;
    }
    /**
     * cette méthode renvoie la moyenne des dépenses par personne
     */
    public double depensesMoyenne()
    {
        double somme = this.totalDepenses(); 
        int nb = listeAmis.size(); 
        if (nb != 0) 
            somme=somme/nb;
        return somme; 
    }

    
    /**
     * la méthode avoirPersonne prend en paramètre une personne 
     * et renvoie l'avoir de cette personne pour le week end.
     */
    public double equilibrer(Personne personne){
        double moyenne = this.depensesMoyenne(); 
        double avoir = moyenne - this.totalDepensesPersonne(personne); 
        return avoir; 
    }
}


