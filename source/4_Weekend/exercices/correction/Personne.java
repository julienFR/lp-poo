public class Personne{
    private String nom;
    
    public Personne(String nom) 
    {
        this.nom = nom;
    }
    
    public String getNom()
    {
        return this.nom;
    }
    
    @Override
    public boolean equals(Object other)
    {
        if (other instanceof Personne){
            Personne autrePersonne = (Personne) other;
            return this.nom.equals(autrePersonne.nom);
        }
        else{
            return false;
        }
    }
    
    @Override
    public int hashCode()
    {
        return this.nom.hashCode();
    }
}
