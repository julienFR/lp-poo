import java.util.List;
import java.util.ArrayList;

public class Groupe{
    
    private String nom;
    private List<Personnage> membresDuGroupe;
    
    public Groupe(String nom){
        this.nom = nom;
        this.membresDuGroupe = new ArrayList<>(); // instancie une nouvelle liste vide
    }
    
    public void ajoute(Personnage perso){
        this.membresDuGroupe.add(perso);
    }
    
    public void ajoute(String nom, int barbe, int oreilles){
        this.ajoute(new Personnage(nom, barbe, oreilles));
    }
    
    @Override
    public String toString(){
        return this.nom+"\n"+this.membresDuGroupe.toString();
    }

    List<Personnage> filtreOreille(double tailleMin){
        List<Personnage> res = new ArrayList<>();
        for(Personnage perso : this.membresDuGroupe){
            if (perso.getOreilles()>tailleMin){
                res.add(perso);
            }
        }
        return res;
    }

    List<Personnage> filtreBarbe(double tailleMax){
        List<Personnage> res = new ArrayList<>();
        for(Personnage perso : this.membresDuGroupe){
            if (perso.getBarbe()<tailleMax){
                res.add(perso);
            }
        }
        return res;
    }
    
}
