import java.util.List;
import java.util.ArrayList;

public class MesChampions{
    
    private String nomDuJoueur;
    private List<Champion> mesChampionsFavoris;
    
    public MesChampions(String joueur){
        this.nomDuJoueur = joueur;
        this.mesChampionsFavoris = new ArrayList<>();
    }
    
    public void ajoute(Champion nouveauChampion){
        this.mesChampionsFavoris.add(nouveauChampion);
    }
    
    @Override
    public String toString(){
        return "Champions favoris de +"+this.nomDuJoueur+"\n"+this.mesChampionsFavoris.toString();
    }
}
