.. raw:: html

   <span class="exo"></span>


Trier une liste en utilisant l'ordre naturel
++++++++++++++++++++++++++++++++++++++++++++++

En java, on peut facilement trier selon l’ordre naturel :

.. code:: java

   import java.util.Collections;
   import java.util.ArrayList;
   import java.util.List;

   public class ExecutableTri{
       public static void main(String[] args)
       {
          List<Integer> listeDeNombres = new ArrayList<>();
          listeDeNombres.add(3);
          listeDeNombres.add(1);
          listeDeNombres.add(2);
          Collections.sort(listeDeNombres);
          System.out.println(listeDeNombres);
         // Affiche [ 1, 2, 3]
       }
   }


#. Dans le code précédent, créez une liste de mots : ``["tu", "ne", "sais", "rien" "John", "Snow"]`` puis triez cette liste avec la méthode ``sort()`` de *Collections*.
   Quel est *l'ordre naturel* des String ?


.. admonition:: Ordre naturel

   En java, une classe possède un *ordre naturel* si elle implémente l'interface ``Comparable``


#. Reprenez la classe ``Personnage`` de l'exercice 3.3. et ajoutez le code nécessaire pour que le code suivant compile.

.. code:: java

   import java.util.List;
   import java.util.ArrayList;
   import java.util.Collections;
   
   public class ExecutableTri{
     public static void main(String [] args){
     
          Personnage gimli = new Personnage("Gimli",65,15);       
          Personnage legolas = new Personnage("Legolas",0,35);
          Personnage grandpas = new Personnage("Aragorn", 20, 8);

          List<Personnage> maListe = new ArrayList<>();
          maListe.add(gimli);
          maListe.add(legolas);
          maListe.add(grandpas);
          maListe.add(new Personnage("Boromir",15,49));
          System.out.println("Liste non triée :"+maListe);
          // Liste non triée : [(Gimli, b=65, o=15), (Legolas, b=0, o=35), (Aragorn, b=20, o=8), (Boromir, b=15, o=49)]
          
          Collections.sort(maListe);
          System.out.println("Liste triée :"+maListe);
          // Liste triée :[(Legolas, b=0, o=35), (Boromir, b=15, o=49), (Aragorn, b=20, o=8), (Gimli, b=65, o=15)]
     }
   }


#. Compléter le code de façon à ce que le code s'exécute correctement.



