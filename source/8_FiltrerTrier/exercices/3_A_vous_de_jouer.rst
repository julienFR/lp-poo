.. raw:: html

   <span class="exo"></span>

A vous de jouer !
+++++++++++++++++++

Arthur et Bernadette aiment beaucoup jouer à League Of Legend.
Ils décident de coder une application permettant de gérer leurs champions favoris.

Récupérer les fichiers suivants

:download:`Champion.java <src/Champion.java>`

:download:`Favoris.java <src/Favoris.java>`

:download:`ExecutableLOL.java <src/ExecutableLOL.java>`

:download:`arthur.csv <src/arthur.csv>`

:download:`bernadette.csv <src/bernadette.csv>`


#. Compléter le code de façon à ce qu'il s'exécute correctement.

#. Décommenter les lignes dans l'exécutable et compléter le code de façon à ce que la compilation ne génère plus d'erreur.

#. Compléter le code de façon à ce que le code s'exécute correctement.



