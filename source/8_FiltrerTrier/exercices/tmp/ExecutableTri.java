import java.util.Collections;
import java.util.ArrayList;
import java.util.List;


public class ExecutableTri{
    public static void main(String[] args)
    {
       List<Integer> listeDeNombres = new ArrayList<>();
       listeDeNombres.add(3);
       listeDeNombres.add(1);
       listeDeNombres.add(2);
       Collections.sort(listeDeNombres);
       System.out.println(listeDeNombres);
      // Affiche [ 1, 2, 3]
      List<String> truc = new ArrayList<>();
      truc.add("tu"); truc.add("ne"); truc.add("sais"); truc.add("rien"); truc.add("John"); truc.add("Snow");
      System.out.println("avant tri : " + truc);     
      Collections.sort(truc);
       System.out.println("après tri : " + truc);
      
      
       Personnage gimli = new Personnage("Gimli",65,15);
       Personnage legolas = new Personnage("Legolas",0,35);
       Personnage grandpas = new Personnage("Aragorn", 20, 8);

       List<Personnage> maListe = new ArrayList<>();
       maListe.add(gimli);
       maListe.add(legolas);
       maListe.add(grandpas);
       maListe.add(new Personnage("Boromir",15,49));
       System.out.println("Liste non triée :"+maListe);
       // Liste non triée : [(Gimli, b=65, o=15), (Legolas, b=0, o=35), (Aragorn, b=20, o=8), (Boromir, b=15, o=49)]

       Collections.sort(maListe);
       System.out.println("Liste triée :"+maListe);
       // Liste triée :[(Legolas, b=0, o=35), (Boromir, b=15, o=49), (Aragorn, b=20, o=8), (Gimli, b=65, o=15)]

    }
}
