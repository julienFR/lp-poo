public class Personnage implements Comparable<Personnage>
{
    private String nom;
    private int tailleBarbe;
    private int tailleOreilles;
    
    public Personnage(String nom, int barbe, int oreilles){
        this.nom = nom;
        this.tailleBarbe = barbe;
        this.tailleOreilles = oreilles;
    }
    
    @Override
    public String toString(){
        return "("+this.nom+", b="+this.tailleBarbe+", o="+this.tailleOreilles+")";
    }
    
    // Autres méthodes pas nécessaires ici mais qui pourraient être utiles
    public String getNom(){ 
    return this.nom;
    }
    public int getBarbe(){
        return this.tailleBarbe;
    }
    public int getOreilles(){
        return this.tailleOreilles;
    }
    
    @Override
    public int compareTo(Personnage autrePerso){
        // if (this.tailleBarbe > autrePerso.tailleBarbe)
            // return 12;
        // else if (this.tailleBarbe < autrePerso.tailleBarbe)
            // return -9;
        // else
            // return 0;
        return this.tailleBarbe - autrePerso.tailleBarbe;
            
        
    }
    
}
