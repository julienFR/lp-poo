.. raw:: html

   <span class="exo"></span>


Trier une liste en utilisant un Comparateur
++++++++++++++++++++++++++++++++++++++++++++++

En java, on peut facilement trier selon l’ordre naturel, mais on peut avoir envie de trier selon d'autres critères.
Dans ce cas, on utilise un comparateur.

Par exemple, si je souhaite trier une liste de mots selon la taille croissante des mots, j'utiliserai le code suivant :

.. code:: Java

   import java.util.Collections;
   import java.util.ArrayList;
   import java.util.List;

   public class ExecutableTri{
       public static void main(String[] args)
       {
          List<String> listeDeMots = new ArrayList<>();
          listeDeMots.add("Coucou");
          listeDeMots.add("tout");
          listeDeMots.add("le");
          listeDeMots.add("monde");
          listeDeMots.add("!");
          System.out.println(listeDeMots);
          // [Coucou, tout, le, monde, !]
                
          ComparateurSuivantTaille monComparateurAMoi = new ComparateurSuivantTaille();
          Collections.sort(listeDeMots, monComparateurAMoi);
          System.out.println(listeDeMots);
          // [!, le, tout, monde, Coucou]
       }
   }
   

Le code du comparateur étant le suivant :

.. code:: Java

   import java.util.Comparator;
   
   public class ComparateurSuivantTaille implements Comparator<String>
   {
       @Override
       public int compare(String s1, String s2){
           return s1.length()- s2.length();
       }
   }



#. Compléter la classe ``Personnage`` de façon à ce que le code suivant compile.

.. code:: Java

   import java.util.List;
   import java.util.ArrayList;
   import java.util.Collections;
   
   public class ExecutableTri{
     public static void main(String [] args){
     
          Personnage gimli = new Personnage("Gimli",65,15);       
          Personnage legolas = new Personnage("Legolas",0,35);
          Personnage grandpas = new Personnage("Aragorn", 20, 8);

          List<Personnage> maListe = new ArrayList<>();
          maListe.add(gimli);
          maListe.add(legolas);
          maListe.add(grandpas);
          maListe.add(new Personnage("Boromir",15,49));
          System.out.println("Liste non triée :"+maListe);
          // Liste non triée : [(Gimli, b=65, o=15), (Legolas, b=0, o=35), (Aragorn, b=20, o=8), (Boromir, b=15, o=49)]
          
          Collections.sort(maListe, new ComparateurOreilles());
          System.out.println("Liste triée par oreille croissante :"+maListe);
          
          Collections.sort(maListe, new ComparateurNom());
          System.out.println("Liste triée par ordre alphabetique :"+maListe);
          
     }
   }


#. Compléter le code de façon à ce que le code s'exécute correctement.



