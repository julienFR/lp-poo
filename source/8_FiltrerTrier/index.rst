:orphan:

Filtres et Tris
--------------------------

**Présentation**

   
.. admonition:: Objectifs
   :class: note
   
   * Consolider les notions abordées précédemment
   * Filtrer
   * Trier
   


.. toctree::
   :maxdepth: 2
   :glob:

   exercices/*


