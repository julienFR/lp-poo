.. raw:: html

   <span class="exo"></span>
   

Attributs et associations
++++++++++++++++++++++++++++


On donne les diagrammes des classes `Timer` et `Ampoule` :

.. uml::

   @startuml
      
   skinparam classAttributeIconSize 0
   class Timer {
   - periode : double
   - actif : boolean
   + Timer(periode:double)
   + Timer()
   + getPeriode():double
   + getActivite():boolean
   + setPeriode(nouvellePeriode:double):void
   + activer():void
   + desactiver():void
   + utiliser():void
   + toString():String
   }

   class Ampoule {
   - couleur : int
   - puissance : int
   - eteinte : boolean
   + Ampoule(couleur:int, puissance:int)
   + Ampoule()
   + getCouleur():int
   + getPuissance():int
   + setCouleur(couleur:int):void
   + setPuissance(couleur:int):void
   + allumer():void
   + eteindre():void
   + toString():String   
   }
   
   @enduml
   

Et voici le code JAVA de la classe `Spot`.


.. literalinclude:: ./src/Spot.java
   :language: java


#. Proposez un diagramme de classes UML pour modéliser la classe `Spot` ainsi que ses relations avec les classes `Ampoule` et `Timer`. Comme les deux dernières classes ont été complètement définies plus haut, on ne représentera ici ni leurs attributs ni leurs méthodes.

#.  Avec cette modélisation :

   a) une Ampoule peut-elle avoir accès aux méthodes d'un Spot ?
   #) un Spot peut-il avoir accès aux méthodes d'un Timer ?
   #) un Timer peut-il avoir accès aux méthodes d'une Ampoule ?

