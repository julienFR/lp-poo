public class Spot {
    
    private Ampoule ampoule;
    private Timer timer;
    
    public Spot(int couleur, int puissance, double periode) {
        this.ampoule = new Ampoule(couleur, puissance);
        this.timer = new Timer(periode);
    }
    
    public double getPeriode() { return this.timer.getPeriode(); }
    public int getCouleur() { return this.ampoule.getCouleur(); }   
    public int getPuissance() { return this.ampoule.getPuissance(); }
    public void setPeriode(double periode) { 
        this.timer.setPeriode(periode);
    }
    public void allumer() {
        this.timer.desactiver();
        this.ampoule.allumer();
    }
    public void eteindre() { this.ampoule.eteindre(); }
    private void clignoter() {
        this.ampoule.allumer();
        this.timer.utiliser();
        this.ampoule.eteindre();
        this.timer.utiliser();
    }
    public void clignoter(double duree) {
        this.timer.activer();
        int nbClignotements = (int) (duree / this.getPeriode());
        for (int i = 0; i < nbClignotements; i++) {
            this.clignoter();
        }
        this.timer.desactiver();
    }
    @Override
    public String toString() {
        return "Spot [periode=" + getPeriode() + ", couleur="
            + getCouleur() + ", puissance=" + getPuissance() + "]";
    }
}
