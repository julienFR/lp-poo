import java.util.List;
import java.util.ArrayList;

public class Pokedex{
    
    private String nom;
    private List<Pokemon> monPokedex;
    
    public Pokedex(String nom){
        this.nom = nom;
        this.monPokedex = new ArrayList<>(); // instancie une nouvelle liste vide
    }

    public Pokedex(){
        this("Nouveau Pokedex");
    }
    
    
    public String getNom(){ 
        return this.nom;
    }

    public int nombrePokemons(){ 
        return this.monPokedex.size();
    }

    public int totalPuissance(){
        int total = 0;
        for (Pokemon pokemon : monPokedex){
            total += pokemon.getAttaque();
        }
        return total;
    }
    
    public void ajoute(Pokemon pokemon){
        this.monPokedex.add(pokemon);
    }
    
    public boolean contient(String nomPokemon){
        for (Pokemon pokemon : monPokedex){
            if (nomPokemon.equals(pokemon.getNom()))
                return true;
        }
        return false;
    }
    
    @Override
    public String toString(){
        return this.nom+"\n"+this.monPokedex.toString();
    }
}
