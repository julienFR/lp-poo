import java.util.List;
import java.util.ArrayList;

public class Groupe{
    
    private String nom;
    private List<Personnage> membresDuGroupe;
    
    public Groupe(String nom){
        this.nom = nom;
        this.membresDuGroupe = new ArrayList<>(); // instancie une nouvelle liste vide
    }
    
    public void ajoute(Personnage perso){
        this.membresDuGroupe.add(perso);
    }
    
    public void ajoute(String nom, int barbe, int oreilles){
       // this.membresDuGroupe.add(new Personnage(String nom, int barbe, int oreilles));
        this.ajoute(new Personnage(nom, barbe, oreilles));
    }
    
    @Override
    public String toString(){
        // String affichage = this.nom+"\n";
        // for (Personnage perso : membresDuGroupe){
            // affichage += perso.toString();
        // }
        // return affichage;
        
        return this.nom+"\n"+this.membresDuGroupe.toString();
        
        
    }
}
