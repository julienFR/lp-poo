public class Pokemon
{
    private String nom;
    private double poids;
    private int valeurAttaque;
    
    public Pokemon(String nom, double poids, int valeurAttaque){
        this.nom = nom;
        this.poids = poids;
        this.valeurAttaque = valeurAttaque;
    }
    
    public String getNom(){ 
        return this.nom;
    }
    public double getPoids(){
        return this.poids;
    }
    public int getAttaque(){
        return this.valeurAttaque;
    }
    
    @Override
    public String toString(){
        return this.nom+" est un pokemon de "+this.poids+" kg et sa valeur d'attaque est "+this.valeurAttaque;
    }
}
