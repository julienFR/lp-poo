.. raw:: html

   <span class="exo"></span>
   

Pokedex et pokemons
++++++++++++++++++++++++++++

.. figure:: ../../images/bulbizarre.png
   :height: 5em
   :align: right
   
On donne la classe executable suivante :

.. literalinclude:: ./src/ExecutablePokemon.java
   :language: java

#. Identifiez les classes nécessaires ainsi que leurs attributs et leurs méthodes. Elaborer un diagramme de classes.

#.  Ecrire le code des classes précédentes.
