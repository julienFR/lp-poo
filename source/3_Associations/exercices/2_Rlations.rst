.. raw:: html

   <span class="exo"></span>
   

Relations entre classes
++++++++++++++++++++++++++++

Pour chacun des énoncés suivants, donnez un diagramme de classes. 


#. Tout écrivain a écrit au moins une oeuvre.
   
   Pour cette question, ne représentez pas les attributs ni les opérations.



#. Un rectangle peut être défini par deux sommets qui sont des points (dans le cas où les côtés sont parfaitement verticaux et horizontaux). On construit un rectangle à partir des coordonnées de deux points. Il est possible de calculer sa surface et son périmètre, ou encore de le translater. Modélisez les attributs et les opérations.
